package fr.corving.invoice.model;

import java.util.List;

public class Business {

    private String name;
    private String adress;

    private List<Estimate> estimates;

    public Business(String name, String adress, List<Estimate> estimates) {
        this.name = name;
        this.adress = adress;
        this.estimates = estimates;
    }

    public Business() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public List<Estimate> getEstimates() {
        return estimates;
    }

    public void setEstimates(List<Estimate> estimates) {
        this.estimates = estimates;
    }
}
