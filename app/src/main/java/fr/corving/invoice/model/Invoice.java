package fr.corving.invoice.model;

public class Invoice extends Estimate{

    private Boolean signature;
    private Boolean isPaid;

    public Invoice(){}

    public Invoice(String title, String object,double price, Boolean signature, Boolean isPaid) {
        super(title, object, price);
        this.signature = signature;
        this.isPaid = isPaid;

        setStateBl(StateBl.INVOICE);
    }

    public Invoice(String title, String object, double price) {
        super(title, object, price);
    }

    public Boolean getSignature() {
        return signature;
    }

    public void setSignature(Boolean signature) {
        this.signature = signature;
    }

    public Boolean getPaid() {
        return isPaid;
    }

    public void setPaid(Boolean paid) {
        isPaid = paid;
    }
}
