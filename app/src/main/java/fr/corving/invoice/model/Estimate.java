package fr.corving.invoice.model;


public class Estimate {
    private String title;
    private String object;
    private StateBl stateBl;
    private double price;

    public Estimate(String title, String object,double price) {
        this.title = title;
        this.object = object;
        this.stateBl = StateBl.ESTIMATE;
        this.price = price;
    }

    public Estimate() {
     }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }
    
    public StateBl getStateBl() {
        return stateBl;
    }

    public void setStateBl(StateBl stateBl) {
        this.stateBl = stateBl;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public static Invoice convertEstimateToInvoice(Estimate estimate) {
        return new Invoice(
                estimate. getTitle(),
                estimate.getObject(),
                estimate.getPrice(),
                false,
                false
        );
    }
}
