package fr.corving.invoice.activity.business;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.corving.invoice.MainActivity;
import fr.corving.invoice.R;
import fr.corving.invoice.adapter.BusinessAdapter;
import fr.corving.invoice.adapter.InvoiceAdapter;
import fr.corving.invoice.model.Business;
import fr.corving.invoice.model.Estimate;
import fr.corving.invoice.model.Invoice;

public class visuBusinessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visu_business);

        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference mUsersReference = mDatabase.getReference("business");
        List<Business> businessLst  = new ArrayList<>();

        mUsersReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    Business business = userSnapshot.getValue(Business.class);

                    System.out.println("Name business: " +business.getName());

                    businessLst.add(new Business(
                            business.getName(),
                            business.getAdress(),
                            null
                    ));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("Firebase", "loadUsers:onCancelled", databaseError.toException());
            }
        });

        System.out.println(businessLst);
        System.out.println("maintenant");

        //recup le recycler
        RecyclerView recyclerView = findViewById(R.id.myLsBusiness);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(new BusinessAdapter(businessLst));


        Button btnBckHome = findViewById(R.id.btnBckToHome);
        btnBckHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
    }
}