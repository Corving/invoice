package fr.corving.invoice.activity.estimate;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.DriverManager;

import fr.corving.invoice.MainActivity;
import fr.corving.invoice.R;
import fr.corving.invoice.model.Estimate;

public class createEstimate extends AppCompatActivity {


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_estimate);

        Button btnCreateEstimate = findViewById(R.id.btnGenerateEstimate);
        btnCreateEstimate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditText inputTitle =  findViewById(R.id.inputTitle);
                String strTitle = String.valueOf(inputTitle.getText());
                EditText inputObject =  findViewById(R.id.inputObject);
                String strObjet = String.valueOf(inputObject.getText());

                EditText inputPrice =  findViewById(R.id.inputPrice);
                double doublePrice = Double.valueOf(String.valueOf(inputPrice.getText()));
                Estimate newEstimate = new Estimate(
                        strTitle,
                        strObjet,
                        doublePrice
                );

                DatabaseReference estimateRef = database.getReference("estimate");
                String estimateId = estimateRef.push().getKey();
                estimateRef.child(estimateId).setValue(newEstimate);
                CharSequence text = "Devis créer!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(createEstimate.this, text, duration);
                toast.show();

                finish();
                Intent homepage = new Intent(createEstimate.this, MainActivity.class);
                startActivity(homepage);
            }
        });

        Button btnBckToHome = findViewById(R.id.btnBckToHome);
        btnBckToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
    }
}