package fr.corving.invoice.activity.invoice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.corving.invoice.MainActivity;
import fr.corving.invoice.R;
import fr.corving.invoice.adapter.InvoiceAdapter;
import fr.corving.invoice.model.Estimate;
import fr.corving.invoice.model.Invoice;

public class visuInvoiceActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visu_invoice);

        List<Invoice> invoiceLst  = Arrays.asList(
                new Invoice("EDF","Install",90, false,false),
                new Invoice("Veolia","migration",200, true,true)
        );

        //recup le recycler
        RecyclerView recyclerView = findViewById(R.id.myLstInvoice);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(new InvoiceAdapter(invoiceLst));


        Button btnBckHome = findViewById(R.id.btnBckToHome);
        btnBckHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
    }
}