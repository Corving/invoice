package fr.corving.invoice.activity.estimate;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import fr.corving.invoice.MainActivity;
import fr.corving.invoice.R;
import fr.corving.invoice.adapter.EstimateAdapter;
import fr.corving.invoice.model.Estimate;

public class visuEstimateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visu_estimate);

        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference mUsersReference = mDatabase.getReference("estimate");
        List<Estimate> estimatesLst  = new ArrayList<>();

        mUsersReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    Estimate estimate = userSnapshot.getValue(Estimate.class);
                    // Traitez ou affichez l'utilisateur ici
                    System.out.println("Titre: "+estimate.getTitle());
                    System.out.println("objet: "+ estimate.getObject());
                    System.out.println("Price: " + estimate.getPrice());
                    estimatesLst.add(estimate);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("Firebase", "loadUsers:onCancelled", databaseError.toException());
            }
        });

        //recup le recycler
        RecyclerView recyclerView = findViewById(R.id.myLstEstimate);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(new EstimateAdapter(estimatesLst));


        Button btnBckHome = findViewById(R.id.btnBckToHome);
        btnBckHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
    }
}