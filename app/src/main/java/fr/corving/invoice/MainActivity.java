package fr.corving.invoice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import fr.corving.invoice.activity.business.visuBusinessActivity;
import fr.corving.invoice.activity.estimate.createEstimate;
import fr.corving.invoice.activity.invoice.visuInvoiceActivity;
import fr.corving.invoice.model.Estimate;

public class MainActivity extends AppCompatActivity {

    List<Estimate> estimateList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnVisuInvoice = findViewById(R.id.btnVisuInvoice);
        Button btnCreateEstimate = findViewById(R.id.btnCreateEstimate);

        btnVisuInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), visuInvoiceActivity.class));
            }
        });

        btnCreateEstimate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), createEstimate.class));
            }
        });
    }
}