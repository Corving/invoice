package fr.corving.invoice.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.corving.invoice.R;
import fr.corving.invoice.model.Business;
import fr.corving.invoice.model.Invoice;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.viewHolder> {

    List<Invoice> invoiceList;

    public InvoiceAdapter(List<Invoice> invoiceList) {
        this.invoiceList = invoiceList;
    }

    public static class viewHolder extends RecyclerView.ViewHolder{

        private final TextView invoiceNameTitleTextView;
        private final TextView invoiceNameObjectTextView;
        private final TextView invoiceNamePriceTextView;
        public viewHolder(@NonNull View itemView) {
            super(itemView);
            invoiceNameTitleTextView = itemView.findViewById(R.id.invoiceNameTitle);
            invoiceNameObjectTextView = itemView.findViewById(R.id.invoiceNameObject);
            invoiceNamePriceTextView = itemView.findViewById(R.id.invoiceNamePrice);
        }
    }

    @Override
    public InvoiceAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invoice,parent,false);
        return new InvoiceAdapter.viewHolder(view);
    }

    @Override
    public void onBindViewHolder(InvoiceAdapter.viewHolder holder, int position) {
        Invoice invoice = invoiceList.get(position);
        holder.invoiceNameTitleTextView.setText(invoice.getTitle());
        holder.invoiceNameObjectTextView.setText(invoice.getObject());
        holder.invoiceNamePriceTextView.setText(String.valueOf(invoice.getPrice()));
    }

    @Override
    public int getItemCount() {
        return invoiceList.size();
    }
}
