package fr.corving.invoice.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.corving.invoice.R;
import fr.corving.invoice.model.Business;
import fr.corving.invoice.model.Estimate;

public class BusinessAdapter  extends RecyclerView.Adapter<BusinessAdapter.viewHolder> {

    private List<Business> lstProduits;

    public BusinessAdapter(List<Business> lstProduits) {
        this.lstProduits = lstProduits;
    }

    public static class viewHolder extends RecyclerView.ViewHolder{

        private final TextView nomProduitTextView;
        public viewHolder(@NonNull View itemView) {
            super(itemView);
            nomProduitTextView = itemView.findViewById(R.id.nameBusiness);

        }
    }

    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_business,parent,false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {
        Business produit = lstProduits.get(position);
        holder.nomProduitTextView.setText(produit.getName());
    }

    @Override
    public int getItemCount() {
        return lstProduits.size();
    }

}
