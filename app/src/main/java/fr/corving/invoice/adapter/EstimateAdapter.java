package fr.corving.invoice.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.corving.invoice.R;
import fr.corving.invoice.model.Estimate;
import fr.corving.invoice.model.Invoice;

public class EstimateAdapter extends RecyclerView.Adapter<EstimateAdapter.viewHolder>{

    List<Estimate> estimateLst;

    public EstimateAdapter(List<Estimate> estimateLst) {
        this.estimateLst = estimateLst;
    }

    public static class viewHolder extends RecyclerView.ViewHolder{

        private final TextView estimateObject;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            estimateObject = itemView.findViewById(R.id.estimateObject);

        }
    }

    @Override
    public EstimateAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_estimate,parent,false);
        return new EstimateAdapter.viewHolder(view);
    }

    @Override
    public void onBindViewHolder(EstimateAdapter.viewHolder holder, int position) {
        Estimate estimate = estimateLst.get(position);
        holder.estimateObject.setText(estimate.getObject());
    }

    @Override
    public int getItemCount() {
        return estimateLst.size();
    }
}
